#ifndef FEATURE_H
#define FEATURE_H
#include <iostream>
#include <vector>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include <boost/filesystem.hpp>
#include "LBP.h"

#include "opencv2/gpu/gpu.hpp"
#include "opencv2/gpu/stream_accessor.hpp"
#include "timer.h"

using namespace std;
using namespace cv;
using namespace boost::filesystem;
using namespace cv::gpu;

class Feature{
public:
	Feature(string);
	void compute(const Mat& query, Mat& feature);
	Mat getColorFeature(Mat);
	Mat getLBPFeature(Mat);
	Mat getHOGFeature(Mat);

private:
	bool mColor;
	bool mLBP;
	bool mHOG;
};

#endif