#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <numeric>
#include <windows.h>
#include "tool.h"
#include "feature.h"
#include "opencv/cv.h"
#include "opencv/ml.h"

using namespace cv;

CvSVM svm;
struct fileCode{
	int id;
	string filepath;
};

void trainSVM(vector<string>  directories, string dstPath, int classesType, string flag, string svmKernel, vector<fileCode>& filelist) {
	Mat images, labels;
	ifstream temp;
	bool stored = fileExistCheck(("image_mat_" + flag + "_gpu.csv").c_str(), temp);
	//bool stored = false;
	//ofstream out("filelist.txt");

	if (!stored){
		int count = 0;
		for (int i = 0; i < directories.size(); i++){
			vector<string> currentDir = readDir(directories[i]);
			vector<string>::iterator it;
			for (it = currentDir.begin(); it != currentDir.end(); it++){
				//cout << "Reading image: " << *it << " with label: " << i << endl;
				//out << i << "\t" << *it << endl;
				fileCode f;
				f.id = count;
				f.filepath = *it;
				cout << *it << endl;
				filelist.push_back(f);

				Mat img = imread(*it);
				resize(img, img, Size(img.cols / 2, img.rows / 2));

				Feature feature(flag);
				Mat res;
				feature.compute(img, res);
				images.push_back(res.reshape(1, 1));
				if (classesType == 0){
					if (i == 0)
						labels.push_back(Mat(1, 1, CV_32F, Scalar(i)));
					else labels.push_back(Mat(1, 1, CV_32F, Scalar(1)));
				}
				else if (classesType == 1){
					labels.push_back(Mat(1, 1, CV_32F, Scalar(i)));
				}
				count++;
			}

		}
		//out.close();
		ofstream outImage("Result/image_mat_" + flag + "_gpu.csv");
		outImage << cv::format(images, "CSV") << endl;
		outImage.close();

		ofstream outLabel("Result/label_mat_" + flag + "_gpu.csv");
		outLabel << cv::format(labels, "CSV") << endl;
		outLabel.close();
		cout << "Save training data done!" << endl;
	}
	else {//when false, read from csv
		CvMLData imageData;
		string savePath1 = "Result/image_mat_" + flag + "_gpu.csv";
		imageData.read_csv(savePath1.c_str());
		cv::Mat tot1(imageData.get_values(), true);
		images = tot1;

		CvMLData labelData;
		string savePath2 = "Result/label_mat_" + flag + "_gpu.csv";
		labelData.read_csv(savePath2.c_str());
		Mat tot2(labelData.get_values(), true);
		labels = tot2;
	}

	images.convertTo(images, CV_32FC1);
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	if (svmKernel == "rbf")
		params.kernel_type = CvSVM::RBF;
	else if (svmKernel == "linear")
		params.kernel_type = CvSVM::LINEAR;
	else if (svmKernel == "sigmoid")
		params.kernel_type = CvSVM::SIGMOID;
	else if (svmKernel == "poly")
		params.kernel_type = CvSVM::POLY;
	else {
		cout << "Incorrect SVM Kernel Selection" << endl;
		return;
	}
	params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	params.degree = 3;


	svm.train(images, labels, Mat(), Mat(), params);
	svm.save(dstPath.c_str());

}

void generate_words(string dir, Mat& images, vector<string>& files, int clusters) {
	files = readDir(dir);
	ifstream temp;
	if (!fileExistCheck("query.csv", temp)){
		vector<string>::iterator it;
		for (it = files.begin(); it != files.end(); it++){
			cout << "Reading image: " << *it << endl;
			Mat img = imread(*it);
			//resize(img, img, Size(img.cols / 2, img.rows / 2));

			Feature feature("l");
			Mat res;
			feature.compute(img, res);
			images.push_back(res.reshape(1, 1));
		}
		cout << images.rows << " * " << images.cols << endl;
	}
	else {
		CvMLData imageData;
		string savePath1 = "query.csv";
		imageData.read_csv(savePath1.c_str());
		cv::Mat tot1(imageData.get_values(), true);
		images = tot1;
	}
	Mat clusterCenters;
	Mat lables;
	kmeans(images, clusters, lables,
		TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, clusters, 1.0),
		0, 0, clusterCenters);
	cout << clusterCenters.rows << " " << clusterCenters.cols << endl;
	
	FileStorage file("codebook.mat", FileStorage::WRITE);
	file << "code" << clusterCenters; // in codebook, record the 10 cluster centers thus 10*128

	file.release();
}

Mat greenFilter(const Mat& src){
	assert(src.type() == CV_8UC3);
	Mat hsv;
	cvtColor(src, hsv, CV_BGR2HSV);
	Mat  greenOnly;
	inRange(hsv, Scalar(31, 109, 70), Scalar(75, 255, 255), greenOnly);
	//imshow("green", greenOnly);

	return greenOnly;
}

EvalResult loadTest(vector<string>& nFrames, int groundTruth, string route, int classesType, string flag){

	vector<string> testFiles = readDir(route);
	vector<string>::iterator it;
	int right = 0, wrong = 0;
	float response, successRate;
	Mat img, test;
	vector<int> predict;
	vector<Mat> currentfeature;
	//ofstream falsepredict("false_prediction_positive_all.txt");
	for (it = testFiles.begin() + 1; it != testFiles.end(); it++) {
		cout << *it << endl;
		string storePath = *it;
		img = imread(*it);
		resize(img, img, Size(img.cols / 2, img.rows / 2));
		Feature feature(flag);
		Mat res;
		feature.compute(img, res);
		test = res.reshape(1, 1);
		currentfeature.push_back(test);
		response = svm.predict(test);
		predict.push_back(response);

		string temp = *it;
		std::replace(temp.begin(), temp.end(), '\\', '/');
		size_t found = temp.find_last_of('/');
		string fileName = temp.substr(found + 1);

		if (response == 1){
			nFrames.push_back(fileName);
		}

		if (classesType == 0){
			if (groundTruth == 0){
				if (floor(response) == groundTruth)
					right++;
				else{
					wrong++;
					imwrite("FalseNegativeAll/" + fileName, img);
				}
			}
			else {
				if (floor(response) != 0)
					right++;
				else {
					wrong++;
					imwrite("FalsePositiveAll/" + fileName, img);
				}
			}
		}
	}
	//falsepredict.close();
	std::cout << "right: " << right << endl;
	std::cout << "wrong: " << wrong << endl;
	successRate = (float)right / (float)(right + wrong);
	std::cout << "successRate: " << successRate << endl;
	EvalResult eval;
	eval.groundTruth = groundTruth;
	eval.route = route;
	eval.right = right;
	eval.wrong = wrong;
	eval.successRate = successRate;
	eval.predict = predict;
	eval.currentFeature = currentfeature;
	return eval;
}

struct intRange{
	int start;
	int end;
	intRange() :start(-1), end(-1){};
};

vector<intRange> getRange(vector<string> nFiles){
	vector<int> nums;
	for (auto item : nFiles){
		auto pos = item.find_first_of('0');
		string number = item.substr(pos);
		int realNumber = atoi(number.c_str());
		nums.push_back(realNumber);
	}
	vector<intRange> res;

	intRange intrange;
	for (int i = 0; i < nums.size();i++){
		if (i == 0 ||( nums[i] != nums[i - 1]+1)){
			if ( i > 0 )
				res.push_back(intrange);
			intrange.start = nums[i];
			intrange.end = nums[i];
		}
		else if (i > 0 && nums[i] == nums[i - 1] + 1){
			intrange.end = nums[i];
		}
	}
	res.push_back(intrange);
	return res;
}


int mainTrainTest(int argc, char* argv[]) {
	int operation = 2;//1 for training, 2 for testing
	int classesType = 0; // 0 for 2 classes
	string flag = argv[1];
	string svmKernel = argv[2];//linear, rbf, sigmoid, poly

	vector<string> directories;
	string s1 = "E:/HorceRaceClassification/JAPAN/train_test/positive/"; directories.push_back(s1);
	string s2 = "E:/HorceRaceClassification/JAPAN/train_test/negative/"; directories.push_back(s2);
	

	string dstPath = "Result/svm_data_" + flag + "_" + svmKernel;
	if (operation == 1)
		trainSVM(directories, dstPath, classesType, flag, svmKernel, vector<fileCode>());
	else if (operation == 2)
		svm.load(dstPath.c_str());

	std::cout << "Start testing! " << endl;

	ofstream out("Result/evaluation_" + flag + "_" + svmKernel + "_svm.txt");
	
	EvalResult eval;
	
	vector<string> nFiles;

	eval = loadTest(nFiles, 0, "E:/HorceRaceClassification/JAPAN/train_test/test/positive/", classesType, flag);
	out << eval.route << "\t" << eval.right << "\t" << eval.wrong << "\t" << eval.successRate << endl;
	eval = loadTest(nFiles, 1, "E:/HorceRaceClassification/JAPAN/train_test/test/negative/", classesType, flag);
	out << eval.route << "\t" << eval.right << "\t" << eval.wrong << "\t" << eval.successRate << endl;

	out << endl << "# Unusable Frames are: " << nFiles.size() <<  endl;
	for (auto item : nFiles)
		out << item << endl;

	vector<intRange> res = getRange(nFiles);
	for (auto item : res)
		out <<" [ " << item.start <<" - " <<  item.end <<" ] "<< endl;

	out.close();
	return 0;
}

Mat getCodebook() {
	FileStorage file("codebook.mat", FileStorage::READ);
	assert(file.isOpened());
	Mat data;
	file["code"] >> data;
	return data;
}

int isMatch(Mat a, Mat b){
	double minDistance = INT_MAX;
	int minIndex = -1;
	for (int i = 0; i < a.rows; i++){
		double distance = norm(a.row(i) - b);
		if (minDistance > distance){
			minDistance = distance;
			minIndex = i;
		}
	}
	
	return minIndex;
}



int mainCluster(int argc, char* argv[]){
	Mat images;
	vector<string> files;
	int  clusters = 20;
	generate_words("E:/HorceRaceClassification/HONG_KONG/happy_valley_train_test/test_positive_negative/", images, files, clusters);

	
	Mat codebook = getCodebook();
	
	Mat feature(clusters, 1, CV_32F);
	
	
	for (int j = 0; j < images.rows; j++){
		int index = isMatch(codebook, images.row(j));
		feature.at<float>(index, 0)++;
		Mat img = imread(files[j]);
		string f = ExtractFileName(files[j], true);
		imwrite( std::to_string(index)  +"/"+f, img);
			
	}
	
	
	return 0;
}

Mat getFeature(string dir){
	vector<string> files = readDir(dir);
	vector<string>::iterator it;
	Mat images;
	for (it = files.begin(); it != files.end(); it++){
		cout << "Reading image: " << *it << endl;
		Mat img = imread(*it);
		//resize(img, img, Size(img.cols / 2, img.rows / 2));

		Feature feature("l");
		Mat res;
		feature.compute(img, res);
		images.push_back(res.reshape(1, 1));
		
	}
	return images;
}

int mainPCA(int argc, char* argv){
	Mat images, test;
	ifstream temp;
	ifstream temp2;
	if (!fileExistCheck("db.csv",temp) || !fileExistCheck("query.csv",temp2)){
		images = getFeature("E:/HorceRaceClassification/HONG_KONG/happy_valley_train_test/test/positive/");
		test = getFeature("E:/HorceRaceClassification/HONG_KONG/happy_valley_train_test/test/negative/");
		vconcat(images, test, test);

		ofstream outImage("db.csv");
		outImage << cv::format(images, "CSV") << endl;
		outImage.close();

		ofstream outTest("query.csv");
		outTest << cv::format(test, "CSV") << endl;
		outTest.close();
		cout << "Save training data done!" << endl;
	}
	else {//when false, read from csv
		CvMLData imageData;
		string savePath1 ="db.csv";
		imageData.read_csv(savePath1.c_str());
		cv::Mat tot1(imageData.get_values(), true);
		images = tot1;

		CvMLData testData;
		string savePath2 = "query.csv";
		testData.read_csv(savePath2.c_str());
		Mat tot2(testData.get_values(), true);
		test = tot2;
	}
	
	int  maxComponents = 257;
	PCA pca(images, Mat(), CV_PCA_DATA_AS_ROW, maxComponents);
	vector<string> files = readDir("E:/HorceRaceClassification/HONG_KONG/happy_valley_train_test/test_positive_negative/");
	Mat compressed(test.rows, maxComponents, test.type());

	Mat low_zero = pca.project(pca.mean);

	ofstream out("pca_result.txt");
	Mat Low_mean;
	for (int i = 0; i < test.rows; i++)
	{
		Mat vec = test.row(i), coeffs = compressed.row(i), reconstructed;
		// compress the vector, the result will be stored
		// in the i-th row of the output matrix
		pca.project(vec, coeffs);
		// and then reconstruct it
		pca.backProject(coeffs, reconstructed);
		static Mat low_mean = Mat::zeros(reconstructed.size(), reconstructed.type());
		low_mean = low_mean + reconstructed;
		Low_mean = low_mean;
	}
	Low_mean = Low_mean * (1. / test.rows);

	for (int i = 0; i < test.rows; i++)
	{
		Mat vec = test.row(i), coeffs = compressed.row(i), reconstructed;
		// compress the vector, the result will be stored
		// in the i-th row of the output matrix
		pca.project(vec, coeffs);
		// and then reconstruct it
		pca.backProject(coeffs, reconstructed);
		// and measure the error
		//cout << pca.mean.rows << " * " << pca.mean.cols << endl;
		//cout << reconstructed.rows << " * " << reconstructed.cols << endl;
		printf("%d. diff = %g\n", files[i], norm(pca.mean, reconstructed, NORM_L2));
		//out << files[i] << "\t" << norm(pca.mean, reconstructed, NORM_L2) << "\t" << norm(vec, reconstructed, NORM_L2) << "\t";
		out << files[i] << "\t" << norm(Low_mean(Rect(250, 0, 6, 1)), reconstructed(Rect(250, 0, 6, 1)), NORM_L2) << "\t";


		//for (int j = 0; j < pca.mean.cols; j++) {
		//	//out << pca.mean.at<float>(0, j) - reconstructed.at<float>(0, j) << "\t";
		//	out << vec.at<float>(0, j) - reconstructed.at<float>(0, j) << "\t";
		//}
		for (int j = 0; j < maxComponents; j++) {
			out << coeffs.at<float>(0, maxComponents - j - 1) << "\t";
		}

		out << endl;
	}
	out.close();
	return 0;
}

int main(int argc, char* argv[]){
	vector<string> files = readDir("E:/masks/");
	for (auto file : files){
		Mat image = imread(file);
		resize(image, image, Size(image.cols, image.rows));

	}
}