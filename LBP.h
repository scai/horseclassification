#ifndef LBP_H
#define LBP_H
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include "utils.h"
#include "timer.h"
#include <iostream>
#include <string>
#include <math.h>
#include <cmath>
#include "opencv2\gpu\gpu.hpp"
#include "opencv2/gpu/stream_accessor.hpp"
using namespace cv;

#define M_PI 3.14159265
class LBP{
public:
	Mat computeCPU(Mat &, int, int);
	Mat computeGPU(Mat &, int, int);

private:

	void your_lbp(const gpu::PtrStepSz<uchar>& d_src, gpu::PtrStepSz<uchar> d_dst, const size_t numRows, const size_t numCols, gpu::PtrStep<unsigned int> d_hist, float* h_array);//, cudaStream_t s = gpu::StreamAccessor::getStream(gpu::Stream::Null()));
};

#endif