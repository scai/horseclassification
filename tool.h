#ifndef TOOL_H
#define TOOL_H
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <boost\filesystem.hpp>

using namespace cv;
using namespace std;
using namespace boost::filesystem;

struct EvalResult{
	int groundTruth;
	string route;
	int right;
	int wrong;
	float successRate;
	vector<int> predict;
	vector<Mat> currentFeature;

};



vector<string> split(const string&, char);
void write_file(string filename, Mat data);
Mat read_file(string filename);
void debug(string msg, Mat& mat);
string to_string(int i);
vector<string> readDir(string dir);

string base_path(string path);
string ExtractPath(string filename);
string ExtractFileExtention(std::string filename);
string ExtractFileName(std::string filename, bool withExtension = true);
bool fileExistCheck(const char* filename, ifstream& inputfile);


#endif
