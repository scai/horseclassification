#include "LBP.h"
#include <fstream>
#include "opencv2\gpu\gpu.hpp"

//ofstream oo("src.txt");
Mat LBP::computeCPU(Mat& src_in, int radius, int neighbors) {
	// convert src_in(color image) to src(grayscale)
	Mat src(src_in.size(), CV_8U);
	if (src_in.channels() == 3)
		cvtColor(src_in, src, CV_BGR2GRAY);
	else{
		src_in.copyTo(src);
	}
	//oo << src << endl;

	//CV_Assert(src.channels() == 1);

	neighbors = max(min(neighbors, 31), 1); //check the neighbors
	Mat dst = Mat::zeros(src.rows - 2 * radius, src.cols - 2 * radius, CV_32F);
	for (int n = 0; n < neighbors; n++) {
		// sample points
		float x = static_cast<float>(radius)* cos(2.0*M_PI*n / static_cast<float>(neighbors));
		float y = static_cast<float>(radius)* -sin(2.0*M_PI*n / static_cast<float>(neighbors));
		// relative indices
		int fx = static_cast<int>(floor(x));
		int fy = static_cast<int>(floor(y));
		int cx = static_cast<int>(ceil(x));
		int cy = static_cast<int>(ceil(y));
		// fractional part
		float ty = y - fy;
		float tx = x - fx;
		// set interpolation weights
		float w1 = (1 - tx) * (1 - ty);
		float w2 = tx  * (1 - ty);
		float w3 = (1 - tx) *      ty;
		float w4 = tx  *      ty;
		// iterate through your data
		for (int i = radius; i < src.rows - radius; i++) {
			for (int j = radius; j < src.cols - radius; j++) {
				float t = w1*src.at<uchar>(i + fy, j + fx) + w2*src.at<uchar>(i + fy, j + cx) + w3*src.at<uchar>(i + cy, j + fx) + w4*src.at<uchar>(i + cy, j + cx);
				//cout << "t: " << t << endl;
				dst.at<float>(i - radius, j - radius) += (((t > src.at<uchar>(i, j)) && (abs(t - src.at<uchar>(i, j)) > std::numeric_limits<float>::epsilon())) << n);
				//cout << "dst: " << dst.at<float>(i - radius, j - radius) << endl;
			}
		}
	}
	return dst;
}

void calculateWeight(float* h_array, int radius, int neighbors){
	for (int n = 0; n < neighbors; n++) {
		// sample points
		float x = static_cast<float>(radius)* cos(2.0*M_PI*n / static_cast<float>(neighbors));
		float y = static_cast<float>(radius)* -sin(2.0*M_PI*n / static_cast<float>(neighbors));
		// relative indices
		int fx = static_cast<int>(floor(x));
		int fy = static_cast<int>(floor(y));
		int cx = static_cast<int>(ceil(x));
		int cy = static_cast<int>(ceil(y));
		// fractional part
		float ty = y - fy;
		float tx = x - fx;
		// set interpolation weights
		float w1 = (1 - tx) * (1 - ty);
		float w2 = tx  * (1 - ty);
		float w3 = (1 - tx) *      ty;
		float w4 = tx  *      ty;
		// iterate through your data
		h_array[n * 8 + 0] = fx;
		h_array[n * 8 + 1] = fy;
		h_array[n * 8 + 2] = cx;
		h_array[n * 8 + 3] = cy;
		h_array[n * 8 + 4] = w1;
		h_array[n * 8 + 5] = w2;
		h_array[n * 8 + 6] = w3;
		h_array[n * 8 + 7] = w4;
	}
}

Mat LBP::computeGPU(Mat& src_in, int radius, int neighbors) {
	gpu::GpuMat d_src;
	gpu::GpuMat d_src_in;
	d_src_in.upload(src_in);
	cvtColor(d_src_in, d_src, CV_BGR2GRAY);


	gpu::GpuMat d_dst(Size(d_src.cols - 2 * radius, d_src.rows - 2 * radius), CV_8UC1, Scalar(0));
	gpu::GpuMat d_hist(1, 256, CV_32S, Scalar(0));
	float* h_array = new float[neighbors * 8];;

	calculateWeight(h_array, radius, neighbors);


	your_lbp(d_src, d_dst, d_src.rows, d_src.cols, d_hist, h_array);

	/*ofstream lbpmat("lbp_mat_gpu.txt");
	Mat canvas;
	d_dst.download(canvas);*/
	//lbpmat << canvas << endl;

	calcHist(d_dst, d_hist);
	/*gpu::GpuMat d_hist_norm(1,256, CV_32F,Scalar(0.0));
	normalize(d_hist, d_hist_norm , 1, 0, NORM_L2, CV_32F);*/
	Mat dst;
	//d_hist_norm.download(dst);
	d_hist.download(dst);
	/*ofstream lbp_hist("lbp_hist_gpu.txt");
	lbp_hist << dst.t() << endl;*/
	//d_dst.download(dst);
	dst.convertTo(dst, CV_32F);
	dst.at<float>(0, 255) = 0;
	//cout << dst << endl;
	//dst = dst(Rect(7, 7, dst.cols - 2 * 7, dst.rows - 2 * 7));
	/*ofstream lbp_norm("lbp_norm_gpu.txt");
	lbp_norm << dst.t() << endl;*/

	return dst.t();

	/*canvas.convertTo(canvas, CV_32F);
	return canvas;*/
}