#include "feature.h"
#include <opencv/cv.h>
#include <opencv/highgui.h>


Feature::Feature(string flag){
	mColor = false;
	mLBP = false;
	mHOG = false;
	for (int i = 0; i < flag.size(); i++){
		if (flag[i] == 'c') mColor = true;
		else if (flag[i] == 'l') mLBP = true;
		else  if (flag[i] == 'h') mHOG = true;
	}
}

void Feature::compute(const Mat& query, Mat& feature){
	feature = Mat::zeros(1, 1, CV_32F);
	if (mColor){
		Mat color_feature = getColorFeature(query);
		feature.push_back(color_feature);
	}
	if (mLBP){
		Mat lbp_feature = getLBPFeature(query); //CPU version
		vconcat(feature, lbp_feature, feature);
	}
	if (mHOG){
		Mat hog_feature = getHOGFeature(query);
		vconcat(feature, hog_feature, feature);
	}
}

Mat Feature::getColorFeature(Mat image){
	IplImage* src = &IplImage(image);
	IplImage* lab = cvCreateImage(cvGetSize(src), 8, 3);
	IplImage* l = cvCreateImage(cvGetSize(src), 8, 1);
	IplImage* a = cvCreateImage(cvGetSize(src), 8, 1);
	IplImage* b = cvCreateImage(cvGetSize(src), 8, 1);
	IplImage* planes[] = { l, a, b };

	int l_bins = 10, a_bins = 10, b_bins = 10;
	int hist_size[] = { l_bins, a_bins, b_bins };

	float l_ranges[] = { 0, 255 };
	float a_ranges[] = { 0, 255 };
	float b_ranges[] = { 0, 255 };
	float* ranges[] = { l_ranges, a_ranges, b_ranges };

	cvCvtColor(src, lab, CV_RGB2Lab);
	cvCvtPixToPlane(lab, l, a, b, 0);

	CvHistogram *hist = cvCreateHist(3, hist_size, CV_HIST_ARRAY, ranges, 1);
	cvCalcHist(planes, hist, 0, 0);

	Mat mfeatures(l_bins * a_bins*b_bins, 1, CV_32F);
	int i = 0;
	for (int l = 0; i < l_bins; l++){
		for (int a = 0; a < a_bins; a++){
			for (int b = 0; b < b_bins; b++){
				mfeatures.at<float>(i++, 0) = cvQueryHistValue_3D(hist, l, a, b);
			}
		}
	}
	mfeatures.convertTo(mfeatures, CV_32FC1);
	normalize(mfeatures, mfeatures);
	return mfeatures;
}

Mat Feature::getHOGFeature(Mat image) {
	cv::HOGDescriptor hog;
	vector<float> descriptors;
	vector<Point> locs;
	Mat src;
	cvtColor(image, src, CV_BGR2GRAY);
	hog.compute(src, descriptors, Size(32, 32), Size(0, 0), locs);
	descriptors.resize(1000);
	Mat desc(descriptors, true);
	normalize(desc, desc);
	return desc;
}

Mat Feature::getLBPFeature(Mat image){
	LBP filter;
	Mat lbp;
	int operation = 2;
	if (operation == 1){
		lbp = filter.computeCPU(image, 7, 8);
		MatND hist;
		int histSize[] = { 256 };
		//change range
		float min = lbp.at<float>(0, 0);
		float max = lbp.at<float>(0, 0);
		for (int i = 0; i < lbp.rows; i++) {
			for (int j = 0; j< lbp.cols; j++) {
				if (min>lbp.at<float>(i, j)) min = lbp.at<float>(i, j);
				if (max < lbp.at<float>(i, j)) max = lbp.at<float>(i, j);//origin float
			}
		}
		float range[] = { min, max };
		const float* ranges[] = { range };
		int channels[] = { 0 };
		calcHist(&lbp, 1, channels, Mat(), hist, 1, histSize, ranges, true, false);
		normalize(hist, hist);
		//out << hist << endl;
		return hist;
	}
	else {
		Mat lbp = filter.computeGPU(image, 7, 8);
		normalize(lbp, lbp);
		//out << lbp << endl;
		return lbp;
	}
}

