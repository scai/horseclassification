#include "tool.h"

bool fileExistCheck(const char* filename, ifstream& inputfile)
{
	bool flag = true;
	inputfile.open(filename);
	if (!inputfile)
	{
		std::cout << "Loading file failed: " << filename << endl;
		return false;
		inputfile.close();
	}
	return flag;
}

vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

void write_file(string filename, Mat data){
	cout << "write file " << filename << endl;
	FileStorage file(filename, FileStorage::WRITE);
	file << "data" << data;
	file.release();
}

Mat read_file(string filename) {
	cout << "read file " << filename << endl;
	Mat data;
	FileStorage file(filename, FileStorage::READ);
	assert(file.isOpened());
	file["data"] >> data;
	return data;
}

void debug(string msg, Mat& mat) {
	cout << msg << " " << mat.rows << " " << mat.cols;
	cout << " depth " << mat.depth();
	cout << " channels " << mat.channels() << endl;
}

string to_string(int i){
	stringstream ss;
	ss << i;
	return ss.str();
}

string base_path(string path) {
	vector<string> vec = split(path, '/');
	return vec[1];
}

vector<string> readDir(string dir) {
	vector<string> files;
	path p(dir);
	vector<path> v;
	copy(directory_iterator(p), directory_iterator(), back_inserter(v));
	sort(v.begin(), v.end());
	for (vector<path>::const_iterator it = v.begin(); it != v.end(); ++it){
		path p2 = *it;
		//cout << p2 << endl;
		//unsigned pos1 = p2.string().find("\\");
		//unsigned pos2 = p2.string().find(".");
		//string name = p2.string().substr(pos1 + 1, p2.string().length() - pos1 - 1);
		//name------pos2-pos1-1 doesn't contain the suffix
		files.push_back(p2.string());
	}

	return files;
}



string ExtractPath(string filename)
{
	string result;
	unsigned int pos = filename.rfind("\\");
	if (pos == string::npos) result = "";
	else result = filename.substr(0, pos);

	return result;
}

string ExtractFileExtention(std::string filename)
{
	string result = "";
	int pos = filename.rfind(".");
	if (pos != string::npos)
		result = filename.substr(pos + 1, filename.size() - 1);
	return result;
}

string ExtractFileName(std::string filename, bool withExtension)
{
	string result;
	int pos = filename.rfind("\\");
	if (pos == string::npos)
		result = filename;
	else {
		result = filename.substr(pos + 1);
	}
	if (!withExtension) {
		pos = result.rfind(".");
		if (pos != string::npos)
			result = result.substr(0, pos);
	}
	return result;
}