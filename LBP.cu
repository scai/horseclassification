//#include "reference_calc.cpp"
#include "LBP.h"
#include "cuda.h"
#include "utils.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <cuda.h>
#include <cuda_runtime.h>
#include <string>
#include <fstream>
#include "opencv2\gpu\gpu.hpp"


#define M_PI 3.14159265

__global__
void lbp(const gpu::PtrStepSz<uchar> d_src,
gpu::PtrStepSz<uchar> d_dst,
int numRows, int numCols,
int radius, int neighbors, const float* const d_array){
	//int* fx, int* fy, int* cx, int* cy, float *w1, float* w2, float* w3, float* w4){
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;

	//int neighbors = 8, radius = 7;
	if (c >= numCols - radius || r >= numRows - radius || c < radius || r < radius){
		return;
	}

	int code = 0;
	for (int n = 0; n < neighbors; n++) {
		float t = d_array[n * 8 + 4] * d_src(r + d_array[n * 8 + 1], c + d_array[n * 8 + 0]) + d_array[n * 8 + 5] * d_src(r + d_array[n * 8 + 1], c + d_array[n * 8 + 2])
			+ d_array[n * 8 + 6] * d_src(r + d_array[n * 8 + 3], c + d_array[n * 8 + 0]) + d_array[n * 8 + 7] * d_src(r + d_array[n * 8 + 3], c + d_array[n * 8 + 2]);

		code += (((t > d_src(r, c)) && (abs(t - d_src(r, c)) > 1.192093e-07)) << n);
	}
	d_dst(r - radius, c - radius) = code;

}

__global__
void kernel_getHist(const gpu::PtrStepSz<uchar> d_dst, gpu::PtrStep<unsigned int> d_hist, int numRows, int numCols, int* histo)
//void kernel_getHist(const gpu::PtrStep<uchar> d_dst, int numRows, int numCols, int* histo)
{
	const int c = blockIdx.x * blockDim.x + threadIdx.x;
	const int r = blockIdx.y * blockDim.y + threadIdx.y;

	//int neighbors = 8, radius = 7;
	if (c >= numCols - 7 || r >= numRows - 7 || c < 7 || r < 7){
		return;
	}

	atomicAdd(&(unsigned int)d_hist(d_dst(r, c), 0), 1);
}


void LBP::your_lbp(const gpu::PtrStepSz<uchar>& d_src, gpu::PtrStepSz<uchar> d_dst, const size_t numRows, const size_t numCols, gpu::PtrStep<unsigned int> d_hist, float* h_array)
{
	const dim3 blockSize(16, 16);
	dim3 gridSize((d_src.cols + blockSize.x - 1) / blockSize.x, (d_src.rows + blockSize.y - 1) / blockSize.y);

	float* d_array;
	checkCudaErrors(cudaMalloc(&d_array, sizeof(float) * 8 * 8));
	checkCudaErrors(cudaMemcpy(d_array, h_array, sizeof(float) * 8 * 8, cudaMemcpyHostToDevice));



	lbp << <gridSize, blockSize >> >(d_src, d_dst, d_src.rows, d_src.cols, 7, 8, d_array);
	cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}